from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.http import require_POST, require_http_methods
from django.http import JsonResponse
from covid.services import Crawler, get_articles, get_one_article_by_id


def get_all_news(request):

    articles = get_articles()

    return JsonResponse(articles,safe=False)


def get_one_article(request):

    if request.method == 'GET':
        param = request.GET['id']
        article = get_one_article_by_id(param)
        return JsonResponse(article,safe=False)

def home(request):

    articles = get_articles()
    return render(request, 'articles.html', {'articles': articles})

def view_one_article(request):

    param = request.GET['id']
    article = get_one_article_by_id(param)
    return render(request,'article.html',{'article': article})
