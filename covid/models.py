from django.db import models



class News(models.Model):

    title = models.TextField(default='none')
    summary = models.TextField()

    def __str__(self):
        return str(self.id)



