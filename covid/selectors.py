from covid.models import News

def get_article_from_database():

    obj = News.objects.all()
    return obj

def get_one_article_from_database(id):

    obj = News.objects.get(id=id)
    return obj