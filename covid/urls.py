from django.urls import path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('allnews/', views.get_all_news),
    url(r'^news/$', views.get_one_article),
    path('', views.home),
    path('article/', views.view_one_article)
]
