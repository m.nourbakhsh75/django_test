import requests
from bs4 import BeautifulSoup
from covid.models import News
from covid.selectors import get_article_from_database, get_one_article_from_database
from urllib.parse import urlparse

url = 'https://cnn.com/world/live-news/coronavirus-pandemic-05-21-20-intl/index.html'

class Crawler:

    def __init__(self,url):
        self.url = url

    def crawl(self):

        location = "delhi technological university"
        PARAMS = {'address': location}
        r = requests.get(url=self.url,
                        params=PARAMS, headers={'User-Agent': 'Mozilla/5.0'})

        print(r.status_code)
        soup = BeautifulSoup(r.text, 'html.parser')
        domain = urlparse(url).netloc

        if domain == 'cnn.com':
            self.parse_cnn_items(soup)

    def parse_cnn_items(self,soup):

        main_div = soup.find('div', class_='ls-main _30cc0dc7 _0fe074fa')
        articles = main_div.findAll('article')
        for article in articles:
            article_title = article.h2.text
            all_summary = article.find('div', class_='Box-sc-1fet97o-0 render-stellar-contentstyles__Content-sc-9v7nwy-0 ivqjEu').findAll('p')
            summary = ''
            for p in all_summary:
                summary = summary + p.text
            
            save_article(article_title,summary)

    def parse_twitter_items(self,soup):

        timeline = soup.find('section')
        
        print(timeline.prettify())


def save_article(title,summary):
    
    item = News(title=title,summary=summary)
    item.save()


def get_articles():

    crawler = Crawler(url)
    crawler.crawl()

    news = get_article_from_database()
    
    all_news = []
    for n in news:
        obj = {
            'id' : n.id,
            'title' : n.title,
        }
        all_news.append(obj)
    return all_news

def get_one_article_by_id(id):

    news = get_one_article_from_database(id)
    obj = {
        'id': news.id,
        'title': news.title,
        'summary': news.summary
    }
    return obj
